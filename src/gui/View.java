package gui;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import model.ProxyOperazione;

public class View extends JPanel implements Observer {

	private ProxyOperazione proxyOperazione;
	private JTextArea area;

	public View(ProxyOperazione proxyOperazione) {
		super();
		this.proxyOperazione = proxyOperazione;
		creaGrafica();
		proxyOperazione.addObserver(this);
	}

	private void creaGrafica() {
		area = new JTextArea();
		area.setEditable(false);
		add(area);
	}

	@Override
	public void update(Observable o, Object arg) {
		area.setText(proxyOperazione.mostraRisultato());
	}

}
