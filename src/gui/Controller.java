package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import exceptions.MathException;
import model.OperazioneMatematica;
import model.OperazioniDisponobili;
import model.ProxyOperazione;

public class Controller extends JPanel {

	private ProxyOperazione proxyOperazione;
	private JTextField parametro1;
	private JTextField parametro2;
	private JButton invio;
	private JComboBox<Object> box;

	public Controller(ProxyOperazione proxyOperazione) {
		super();
		this.proxyOperazione = proxyOperazione;
		creaGrafica();
		setupInvio();
	}

	private void creaGrafica() {
		parametro1 = new JTextField("Inserisci il primo parametro");
		parametro2 = new JTextField("Inserisci il secondo parametro");
		invio = new JButton("Esegui operazione");
		box = new JComboBox<>(OperazioniDisponobili.getInstance().getOperazioni().toArray());
		setLayout(new GridLayout(4, 1));
		add(parametro1);
		add(parametro2);
		add(box);
		add(invio);
	}

	private void setupInvio() {
		invio.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int p1 = Integer.valueOf(parametro1.getText());
					int p2 = Integer.valueOf(parametro2.getText());
					OperazioneMatematica operazioneMatematicaScelta = (OperazioneMatematica) box.getSelectedItem();
					proxyOperazione.setOperazione(operazioneMatematicaScelta);
					try {
						proxyOperazione.calcola(p1, p2);
						proxyOperazione.update();
					} catch (MathException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, e1.getMessage(), "Errore", JOptionPane.WARNING_MESSAGE);
					}
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, "Inserisci dei numeri interi", "Errore",
							JOptionPane.WARNING_MESSAGE);
				}

			}
		});
	}

}
