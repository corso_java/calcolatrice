package operations;

import exceptions.MathException;
import model.OperazioneMatematica;

public class Divisione implements OperazioneMatematica {

	private double quoziente;
	private double resto;

	@Override
	public void calcola(int a, int b) throws MathException {
		if (b != 0) {
			quoziente = (double) a / b;
			resto = a % b;
		} else {
			throw new MathException("Non puoi dividere per 0");
		}

	}

	@Override
	public String mostraRisultato() {
		return "Quoziente= " + quoziente + " resto= " + resto;
	}

}
