package operations;

import exceptions.MathException;
import model.OperazioneMatematica;

public class PowOperation implements OperazioneMatematica {

	private int elevato;

	@Override
	public void calcola(int a, int b) throws MathException {
		if (a == 0 && b == 0) {
			throw new MathException("Entrambi i numeri sono 0");
		} else {
			elevato = (int) Math.pow(a, b);
		}
	}

	@Override
	public String mostraRisultato() {
		return "L'elevamento di potenza vale " + elevato;
	}

}
