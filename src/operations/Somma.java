package operations;

import model.OperazioneMatematica;

public class Somma implements OperazioneMatematica {

	private int somma;

	@Override
	public void calcola(int a, int b) {
		somma = a + b;
	}

	@Override
	public String mostraRisultato() {
		return "La somma vale " + somma;
	}

	@Override
	public String toString() {
		return "Somma";
	}

}
