package operations;

import model.OperazioneMatematica;

public class Sottrazione implements OperazioneMatematica {

	private int differenza;

	@Override
	public void calcola(int a, int b) {
		differenza = a - b;

	}

	@Override
	public String mostraRisultato() {
		return "La differenza vale " + differenza;
	}

}
