package operations;

import exceptions.MathException;
import model.OperazioneMatematica;

public class FindMin implements OperazioneMatematica {

	private int minimo;

	@Override
	public void calcola(int a, int b) throws MathException {
		minimo = Math.min(a, b);

	}

	@Override
	public String mostraRisultato() {
		return "Il valore minimo è " + minimo;
	}
	
	@Override
	public String toString() {
		return "Trova il minimo";
	}

}
