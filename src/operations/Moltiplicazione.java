package operations;

import model.OperazioneMatematica;

public class Moltiplicazione implements OperazioneMatematica {

	private int prodotto;

	@Override
	public void calcola(int a, int b) {
		prodotto = a * b;

	}

	@Override
	public String mostraRisultato() {
		return "Il prodotto vale " + prodotto;
	}

}
