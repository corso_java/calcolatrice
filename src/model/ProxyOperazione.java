package model;

import java.util.Observable;

import exceptions.MathException;

public class ProxyOperazione extends Observable implements OperazioneMatematica {

	private OperazioneMatematica operazione;

	public ProxyOperazione(OperazioneMatematica operazione) {
		super();
		this.operazione = operazione;
	}

	public ProxyOperazione() {
		super();
	}

	public OperazioneMatematica getOperazione() {
		return operazione;
	}

	public void setOperazione(OperazioneMatematica operazione) {
		this.operazione = operazione;
	}

	@Override
	public void calcola(int a, int b) throws MathException {
		operazione.calcola(a, b);
	}

	@Override
	public String mostraRisultato() {
		return operazione.mostraRisultato();
	}

	public void update() {
		setChanged();
		notifyObservers();
	}
}
