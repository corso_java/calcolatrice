package model;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import operations.Divisione;
import operations.Moltiplicazione;
import operations.Somma;
import operations.Sottrazione;
import utils.ClassFinder;

public class OperazioniDisponobili {

	private List<OperazioneMatematica> operazioni;
	private static OperazioniDisponobili disponibili;

	private OperazioniDisponobili() {
		caricaOperazioni();
	}

	public static OperazioniDisponobili getInstance() {
		if (disponibili == null) {
			disponibili = new OperazioniDisponobili();

		}
		return disponibili;
	}

	private void caricaOperazioni() {
		operazioni = new ArrayList<>();
		List<Class<?>> classes = ClassFinder.find("operations");
		for (Class<?> class1 : classes) {
			try {
				operazioni.add((OperazioneMatematica) class1.getConstructor().newInstance());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	public static OperazioniDisponobili getDisponibili() {
		return disponibili;
	}

	public void setDisponibili(OperazioniDisponobili disponibili) {
		OperazioniDisponobili.disponibili = disponibili;
	}

	public List<OperazioneMatematica> getOperazioni() {
		return operazioni;
	}

}
