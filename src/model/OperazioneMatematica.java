package model;

import exceptions.MathException;

public interface OperazioneMatematica {

	/**
	 * Questo metodo calcola l'operazione matematica tra due numeri interi
	 * 
	 * @param a
	 * @param b
	 */
	public void calcola(int a, int b) throws MathException;

	/**
	 * Questo metodo ritorna il risultato di un'operazione matematica
	 * 
	 * @return
	 */
	public String mostraRisultato();

}
