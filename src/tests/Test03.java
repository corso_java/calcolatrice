package tests;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import utils.ClassFinder;

public class Test03 {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<Class<?>> classes = ClassFinder.find("operations");
		for (Class<?> class1 : classes) {
			System.out.println(class1.getConstructor().newInstance().toString());
		}
	}
}
