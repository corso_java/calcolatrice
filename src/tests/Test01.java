package tests;

import exceptions.MathException;
import model.ProxyOperazione;
import operations.Somma;
import operations.Sottrazione;

public class Test01 {

	public static void main(String[] args) throws MathException {
		ProxyOperazione proxyOperazione = new ProxyOperazione(new Somma());
		proxyOperazione.calcola(4, 0);
		System.out.println(proxyOperazione.mostraRisultato());
		proxyOperazione.setOperazione(new Sottrazione());
		proxyOperazione.calcola(4, 0);
		System.out.println(proxyOperazione.mostraRisultato());
	}
}
