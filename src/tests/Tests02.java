package tests;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Tests02 {
	public static void main(String[] args) {
		JPanel panel = new JPanel();
		panel.setBackground(Color.RED);
		JLabel label = new JLabel("This is my label");
		JTextField field = new JTextField("Inserisci qualcosa");
		JButton button = new JButton("Fai click");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Mi hai cliccato");

			}
		});
		field.setEditable(false);
		panel.add(field);
		panel.add(label);
		panel.add(button);
		JFrame frame = new JFrame("My Title");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel);
		frame.setVisible(true);
		frame.setSize(800, 400);
	}
}
