package tests;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import gui.Controller;
import gui.View;
import model.ProxyOperazione;
import operations.Somma;

public class MainGui {

	public static void main(String[] args) {
		ProxyOperazione proxyOperazione = new ProxyOperazione(new Somma());
		JPanel pannello = new JPanel();
		pannello.setLayout(new GridLayout(2, 1));
		pannello.add(new Controller(proxyOperazione));
		pannello.add(new View(proxyOperazione));
		JFrame frame = new JFrame("Calcolatrice");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(pannello);
		frame.setSize(800, 400);
		frame.setVisible(true);
	}
}
